# Test Challenge: Tabulator Component

Implement Tabulator component(s) in React + TypeScript (from scratch).
Use a starter you prefer: `create-next-app`, `create-react-app`, etc.
Optionally: deploy a working instance somewhere.
Styling / UI is not necessary.

Please publish your solution on GitHub / GitLab with all the necessary documentation.
Tasks 1 and 2 are required. Bonus tasks are optional but each one of them adds scores.

Terms: we expect you to finish the exercise in **1 week** after you receive it.
In terms of hours the estimation is somewhere between 4–8 hours. Depending on your skills and the # of bonus tasks you take.

## Preview

[https://carlosmonti.gitlab.io/tabulator/](https://carlosmonti.gitlab.io/tabulator/)

[![TwjemfcwJZ.gif](https://s8.gifyu.com/images/TwjemfcwJZ.gif)](https://gifyu.com/image/SKo8H)

## Task 1

Implement `Tab` and `TabPane` components with the following API:

### Interface

`components/Tab.tsx`:

```tsx
export type TabProps = {
  children : React.ReactElement<TabPaneProps>[]
}

function Tab(props : TabProps) : JSX.Element {
  ??
}

function TabPane() : JSX.Element {
  ??
}

// export both to be accessible as `Tab` and `Tab.Pane`
```

### Usage

`App.tsx (whatever module name)`:

```tsx
<Tab>
  <Tab.Pane title="First Pane">
    First Pane Body
  </Tab.Pane>

  <Tab.Pane title="Second Pane">
    Second Pane Body
  </Tab.Pane>
</Tab>
```

Make sure only the current pane is rendered!

## Task 2

Support uncontrolled and controlled modes in parallel:

```tsx
// Uncontrolled
<Tab initialActive={0}>
  <Tab.Pane title="A">...</Tab.Pane>
  <Tab.Pane title="B">...</Tab.Pane>
</Tab>

// Controlled
<Tab active={active} onActiveChange={onActiveChange}>
  <Tab.Pane title="A">...</Tab.Pane>
  <Tab.Pane title="B">...</Tab.Pane>
</Tab>
```

Update types accordingly. Passing `active` and `initialActive` props at the same time should be a type error.

## Bonus Task 1 (react+)

Implement another version of `Tab`, say `Tab2` which renders each pane just once.
Tip: like you'd do it in CSS with `display: hidden`.
As previously, the initial render phase should not bring non-active panes to the DOM.

## Bonus Task 2 (ts+)

Update types to prevent out-of-bound `active / initialActive` values (if it's technically possible).
Like the following should be a type error:

```tsx
<Tab initialActive={33}>
  <Tab.Pane title="A">-a-</Tab.Pane>
  <Tab.Pane title="B">-b-</Tab.Pane>
</Tab>
```

If it's not possible, provide explanations / links.

## Bonus Task 3 (qa+)

Write tests for your component(s).

## Bonus Task 4 (fe+)

Publish your component as an NPM library. Make sure it can be used in a TypeScript-based project.

---

## Documentation

* Node version: v14
* NPM version: v6

Open CLI and input

```bash
git clone git@gitlab.com:carlosmonti/tabulator.git
```

```bash
cd tabulator && npm install && npm start
```

<br />

### Now open your browser @ [http://localhost:3000/tabulator](http://localhost:3000/tabulator)

<br />

### Unit Tests

```bash
npm test
```

### Task 1 resolution

* ✅ The only tab panel rendered is the active/selected panel.
* ✅ Both components `Tab` & `TabPane` are accesible through `Tab` & `Tab.Pane` respectively.

### Task 2 resolution

* ✅ Supported both controlled and uncontrolled components in parallel.
* ✅ Passing active and initialActive props at the same time throws a type and a javascript error.

### Bonus

* ✅ Only one tab panel is rendered.
* ✅ Basic unit tests written.
* ✅ Deployed working instance.
