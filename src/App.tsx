import React from 'react';
import Tab from './components/Tab/Tab';

const App: React.FC = () => {
  const [active, setActive] = React.useState("Controlled A");

  const onActiveChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setActive(value);
  };

  return (
    <div className="app">
      <section className="task-01">
        <h1>Task 1</h1>
        <Tab>
          <Tab.Pane title="First Pane">First Pane Body</Tab.Pane>
          <Tab.Pane title="Second Pane">Second Pane Body</Tab.Pane>
        </Tab>
      </section>
      <section className="task-02">
        <h1>Task 2</h1>
        <div className="sub-task">
          <h2>Uncontrolled</h2>
          <Tab initialActive={1}>
            <Tab.Pane title="Uncontrolled A">something A</Tab.Pane>
            <Tab.Pane title="Uncontrolled B">something B</Tab.Pane>
          </Tab>
        </div>
        <div className="sub-task">
          <h2>Controlled</h2>
          <Tab active={active} onActiveChange={onActiveChangeHandler}>
            <Tab.Pane title="Controlled A">something A</Tab.Pane>
            <Tab.Pane title="Controlled B">something B</Tab.Pane>
          </Tab>
        </div>
      </section>
    </div>
  );
};

export default App;
