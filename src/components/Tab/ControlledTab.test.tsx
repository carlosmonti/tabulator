import React from 'react';
import { render } from '@testing-library/react';
import ControlledTab from './ControlledTab';
import TabPane from './TabPane';

export const SimpleControlledTab: React.FC = () => {
  const defaultProp = "Controlled B";
  const [active, setActive] = React.useState(defaultProp);

  const onClickHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setActive(value);
  };

  return (
    <ControlledTab active={active} onActiveChange={onClickHandler}>
      <TabPane title="Controlled A">something A</TabPane>
      <TabPane title="Controlled B">something B</TabPane>
    </ControlledTab>
  );
};

test('should render correctly', () => {
  const { container } = render(<SimpleControlledTab />);

  expect(container).toMatchSnapshot();
});

test('should render a ".tab-header" container', () => {
  const { container } = render(<SimpleControlledTab />);

  expect(container.querySelector('.tab-header')).toBeInTheDocument();
});

test('should render only one ".tab-content" container', () => {
  const { container } = render(<SimpleControlledTab />);

  expect(container.getElementsByClassName('tab-content')).toHaveLength(1);
  expect(container.querySelector('.tab-content')).toBeInTheDocument();
});
