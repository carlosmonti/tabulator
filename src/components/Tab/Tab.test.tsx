import { render } from '@testing-library/react';

import Tab from './Tab';
import { SimpleControlledTab } from './ControlledTab.test';
import { SimpleTabComponent } from './UncontrolledTab.test';

test('should render an Uncontrolled Tab without initialization', () => {
  const { container } = render(<SimpleTabComponent />);

  expect(container).toMatchSnapshot();
});

test('should render an Uncontrolled Tab with initialization', () => {
  const { container } = render(
    <Tab initialActive={1}>
      <Tab.Pane title="Uncontrolled A">something A</Tab.Pane>
      <Tab.Pane title="Uncontrolled B">something B</Tab.Pane>
    </Tab>
  );

  expect(container).toMatchSnapshot();
});

test('should render an Uncontrolled Tab with initialization', () => {
  const { container } = render(<SimpleControlledTab />);

  expect(container).toMatchSnapshot();
});
