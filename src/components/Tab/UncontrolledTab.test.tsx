import React from 'react';
import { render } from '@testing-library/react';
import UncontrolledTab from './UncontrolledTab';
import TabPane from './TabPane';

export const SimpleTabComponent: React.FC = () => (
  <UncontrolledTab>
    <TabPane title="Uncontrolled A">something A</TabPane>
    <TabPane title="Uncontrolled B">something B</TabPane>
  </UncontrolledTab>
);

test('should render a ".tab-header" container', () => {
  const { container } = render(<SimpleTabComponent />);

  expect(container.querySelector('.tab-header')).toBeInTheDocument();
});

test('should render only one ".tab-content" container', () => {
  const { container } = render(<SimpleTabComponent />);

  expect(container.getElementsByClassName('tab-content')).toHaveLength(1);
  expect(container.querySelector('.tab-content')).toBeInTheDocument();
});

test('should render two tabs with first tab active', () => {
  const { container } = render(<SimpleTabComponent />);

  const tabs = container.querySelectorAll('.tab-title');
  const firstTab = tabs[0];

  expect(tabs).toHaveLength(2);
  expect(firstTab.classList.contains('active')).toBeTruthy();
  expect(container).toMatchSnapshot();
});

test('should render two tabs with second tab active using "initialActive"', () => {
  const { container } = render(
    <UncontrolledTab initialActive={1}>
      <TabPane title="Uncontrolled A">something A</TabPane>
      <TabPane title="Uncontrolled B">something B</TabPane>
    </UncontrolledTab>
  );

  const tabs = container.querySelectorAll('.tab-title');
  const firstTab = tabs[0];
  const secondTab = tabs[1];

  expect(tabs).toHaveLength(2);
  expect(firstTab.classList.contains('active')).toBeFalsy();
  expect(secondTab.classList.contains('active')).toBeTruthy();
  expect(container).toMatchSnapshot();
});
