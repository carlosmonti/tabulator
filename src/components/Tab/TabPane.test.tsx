import { render, screen } from '@testing-library/react';
import TabPane from './TabPane';

test('should render tab pane', () => {
  const title = "Some title";
  const content = "Some content";

  const { container } = render(
    <TabPane title={title}>{content}</TabPane>
  );

  expect(screen.getByText(content)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
