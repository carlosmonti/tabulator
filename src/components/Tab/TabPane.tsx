export type TabPaneProps = {
  title: string;
  children: React.ReactNode;
};

const TabPane = (props: TabPaneProps) : JSX.Element => {
  const { children } = props;

  return <div className="tab-pane">{children}</div>;
};

export default TabPane;
