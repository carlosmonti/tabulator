import ControlledTab, { ControlledProps } from "./ControlledTab";
import UncontrolledTab, { UncontrolledProps } from "./UncontrolledTab";
import TabPane, { TabPaneProps } from "./TabPane";

export type TabProps = {
  children : React.ReactElement<TabPaneProps>[];
};

const Tab = (props: TabProps & ControlledProps | UncontrolledProps) : JSX.Element => {
  const { active, children, initialActive, onActiveChange } = props;

  const isControlled = !!active && !!onActiveChange;

  if (!!active && !!onActiveChange && !!initialActive) throw new Error("Something bad happend");

  return (
    <div className="tab-container">
      {isControlled && <ControlledTab active={active} onActiveChange={onActiveChange}>{children}</ControlledTab>}
      {!isControlled && <UncontrolledTab initialActive={initialActive}>{children}</UncontrolledTab>}
    </div>
  );
};

Tab.Pane = TabPane;

export default Tab;
