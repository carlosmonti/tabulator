import React from 'react';

import { getTitles, sanitizeString } from "../../utils";
import { TabProps } from './Tab';

export type UncontrolledProps = TabProps & {
  active?: never;
  initialActive?: number;
  onActiveChange?: undefined;
};

const UncontrolledTab = (props: UncontrolledProps) => {
  const { children, initialActive } = props;
  const safeInitialActive = !initialActive || children.length < initialActive ? 0 : initialActive;
  const [activeTab, setActiveTab] = React.useState(safeInitialActive);

  const titles = getTitles(children);

  // Click handler to set the active tab
  const onClickHandler = (tabNumber: number) => {
    setActiveTab(tabNumber);
  };

  return (
    <>
      <ul className="tab-header">
        {titles.map((title, index) => {
          const isActive = activeTab === index;
          const headerClass = `tab-title ${isActive ? "active" : ""}`;
          const key = sanitizeString(title);

          return (
            <li
              key={key}
              className={headerClass}
              onClick={() => onClickHandler(index)}
            >
              {title}
            </li>
          );
        })}
      </ul>

      <article className={`tab-content tab-number-${activeTab}`}>
        {children[activeTab]}
      </article>
    </>
  );
};

export default UncontrolledTab;
