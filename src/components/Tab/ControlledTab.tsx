import React from 'react';

import { getTitles, sanitizeString } from "../../utils";
import { TabProps } from './Tab';

export type ControlledProps = TabProps & {
  active: string;
  initialActive?: never;
  // eslint-disable-next-line no-unused-vars
  onActiveChange: (event: React.ChangeEvent<HTMLInputElement>) => void
};

const ControlledTab = (props: ControlledProps) => {
  const { active, children, onActiveChange } = props;

  // Get titles from tab panes
  const titles = getTitles(children);

  return (
    <>
      <div className="tab-header">
        {titles.map((title, index) => {
          const id = `tab-${index + 1}`;
          const isRadioChecked = sanitizeString(title) === sanitizeString(active);

          return (
            <div className="tab-title" key={id}>
              <input
                checked={isRadioChecked}
                id={id}
                name="title"
                onChange={onActiveChange}
                type="radio"
                value={title}
              />
              <label htmlFor={id}>{title}</label>
            </div>
          );
        })}
      </div>

      <article className="tab-content">
        {children.map((child) => {
          const { title } = child.props;
          const isActive = sanitizeString(title) === sanitizeString(active);

          return isActive ? child : null;
        })}
      </article>
    </>
  );
};

export default ControlledTab;
