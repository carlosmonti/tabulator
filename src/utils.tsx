// Get titles from children
export const getTitles = (titles: React.ReactElement[]) => titles.map((title) => title.props.title);
// Remove spaces and lowercase passed strings
export const sanitizeString = (value: string) => value.toLowerCase().replace(/\s/g, "");
